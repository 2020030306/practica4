arreglo = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

function promedio(arreglo){
    prom=0.0;
    suma=0.0;
    for(i=0;i<20;i++)
    {
        suma=suma+arreglo[i];
    }
    prom=suma/20;
    result=document.getElementById('resultado')
    result.innerHTML="Promedio: " +prom;
}

function pare(arreglo){
    prom=0.0;
    suma=0.0;
    for(i=0;i<20;i++)
    {
        if(arreglo[i]%2==0){
            suma++;
        }  
    }
    result=document.getElementById('resultado')
    result.innerHTML="Cantidad total de pares: " +suma;
}

function ordenar(arreglo){
    arreglo.sort((a,b)=>{
        if(a==b){
            return 0;
        }
        if(a>b){
            return -1;
        }
        return 1;
    })
    result=document.getElementById('resultado')
    result.innerHTML="Ordenados de mayor a menor: " +arreglo;
}

var orden=[];

function llenar(){
    var limite = document.getElementById('limite').value;
    var listanumeros = document.getElementById('numeros');
    var min = 1;
    var max = 50;
    listanumeros.innerHTML = "";
    for(let con = 0; con<limite; con++){
        var x = Math.floor(Math.random()*(max-min+1)+min);
        num=x;

        orden.push(num);
    }
    orden.sort((a,b)=>{
        if(a==b)
        {
            return 0;
        }
        if(a<b){
            return -1;
        }
        return 1;
    })
    for(let con=0;con<limite;con++){
        listanumeros.options[con] = new Option(orden[con], 'valor:'+ con);
    }
    pares(orden);
}

function pares(orden) {
    var limite = document.getElementById('limite').value;
    par = 0;
    impar = 0;
    for (i = 0; i < limite; i++) {
        if (orden[i] % 2 == 0) {
            par++;
        }
        else {
            impar++;
        }
    }
    var popar = 0.0;
    popar = par / limite * 100;
    var poimpar = 0.0;
    poimpar = impar / limite * 100;

    document.querySelector("#porPares").innerHTML = popar.toFixed(0) + "%";
    document.querySelector("#porImpares").innerHTML = poimpar.toFixed(0) + "%";

    popar=popar.toFixed(0);
    poimpar=poimpar.toFixed(0);
    var dif = 0.0;

    if(popar < poimpar){
        dif = poimpar - popar;
    }if (popar > poimpar) {
        dif = popar - poimpar;
    } 

    if (dif <= 25) {
        document.querySelector("#Simetrico").innerHTML = "Sí es Simétrica";
    } if(dif > 25) {
        document.querySelector("#Simetrico").innerHTML = "No es Simétrica";
    } if (popar == poimpar) {
        document.querySelector("#Simetrico").innerHTML = "Sí es Simétrica";
    }
}